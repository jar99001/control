package com.allander.control;

import android.widget.TextView;

import java.util.logging.Level;
import java.util.logging.Logger;

public class UIOut {

    private static Logger logger = Logger.getLogger("controlLogger");

    public static void setText(final TextView outputTextView, final String outputString) {
        new Runnable() {
            @Override
            public void run() {
                outputTextView.setText(outputString);
            }
        }.run();

    }

    public static void warningText(String message) {
        logger.log(Level.WARNING, message);
    }

    public static void errorText(String message) {
        logger.log(Level.SEVERE, message);
    }

    public static void infoText(String message) {
        logger.log(Level.INFO, message);
    }

    public static void debugText(String message) {
        logger.log(Level.FINEST, message);
    }
}
