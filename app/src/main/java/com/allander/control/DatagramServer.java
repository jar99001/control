package com.allander.control;


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class DatagramServer extends Thread {
    DatagramSocket socket;

    public DatagramServer(DatagramSocket socket) {
        this.socket = socket;
    }

    public void run() {
        UIOut.infoText("Starting up datagram server.");
        byte[] buffer = new byte[100];
        DatagramPacket input = new DatagramPacket(buffer,buffer.length);
        try {
            socket.receive(input);
            UIOut.infoText(input.getData().toString());
        } catch (IOException e) {
            UIOut.warningText("Cant receive message: " + e);
        }
    }

}
