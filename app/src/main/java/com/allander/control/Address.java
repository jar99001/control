package com.allander.control;

import java.net.InetAddress;

public class Address {

    public InetAddress inetAddress;
    public Integer port;

    public Address(InetAddress inetAddress, int port) {
        this.inetAddress = inetAddress;
        this.port = port;
    }
}
