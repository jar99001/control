package com.allander.control;

import android.provider.ContactsContract;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Connection implements Serializable {
    Long idNumber;
    DatagramSocket friendsSocket;

    Set<Address> maybeaddress = new HashSet<>();



    public Connection(Long idNumber) {
        this.idNumber = idNumber;
        loadStoredSettings();
    }

    private void loadStoredSettings() {
        try {
            maybeaddress.add(new Address(InetAddress.getByName("127.0.0.1"), 5555));
            maybeaddress.add(new Address(InetAddress.getByName("127.0.0.1"), 5556));
            maybeaddress.add(new Address(InetAddress.getByName("127.0.0.1"), 5557));
        } catch (UnknownHostException e) {
            UIOut.errorText("Cant pars internet address from string.");
        }

        try {
            this.friendsSocket = new DatagramSocket();
        } catch (SocketException e) {
            UIOut.errorText("Failed to create friend socket for friend id: " + this.idNumber);
        }
    }


    public void reConnect() {
        byte[] buffer = ("Hello friend").getBytes();

        for (Address address : maybeaddress) {
            try {
                DatagramPacket packet = new DatagramPacket(buffer,0, buffer.length, address.inetAddress, address.port);
                friendsSocket.send(packet);
            } catch (IOException e) {
                UIOut.warningText("Unabled to send packet to " + address.inetAddress + ":" + address.port);
            }
        }
    }


}
