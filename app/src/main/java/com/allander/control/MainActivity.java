package com.allander.control;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    TextView debugOutput;
    Button connectButton;
    StringBuilder outString = new StringBuilder();
    DatagramSocket outputSocket;
    Set<Integer> outputPorts = new HashSet<>();
    DatagramServer datagramServer;

    Set<Connection> connections = new HashSet<>();

    private void loadUsersAndSettings() {
        outputPorts.add(5556);
        outputPorts.add(5557);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        debugOutput = findViewById(R.id.debugOutput);
        connectButton = findViewById(R.id.connectButton);

        loadUsersAndSettings();

        connectButton.setOnClickListener(view -> {
            UIOut.setText(debugOutput, outString.append("Connecting...\n").toString());
            establishDatagramServer();
            reConnect();
        });

    }

    private void establishDatagramServer() {
        for (Integer port : outputPorts) {
            try {
                this.outputSocket = new DatagramSocket(port);
                UIOut.infoText("Successfully connected to port " + port + " local ip is " + this.outputSocket.getInetAddress());
                break;
            } catch (SocketException e) {
                UIOut.warningText("Cant connect to port " + port);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    UIOut.errorText("Failed to run Thread.sleep");
                }
            }
        }
        datagramServer = new DatagramServer(outputSocket);
        datagramServer.start();
    }

    private void reConnect() {
        connections.add(new Connection((long) 1));
        for (Connection friend: connections) {
            friend.reConnect();
        }
    }

}
